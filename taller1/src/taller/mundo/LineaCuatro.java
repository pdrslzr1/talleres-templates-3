package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;
	
	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;
	
	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;
	
	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;
	
	/**
	 * El número del jugador en turno
	 */
	private int turno;

	private int fichasParaGanar;
	
	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol, int pFichas)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		atacante = jugadores.get(turno).darNombre();
        fichasParaGanar = pFichas;
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}
	
	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}
	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}
	
	/**
	 * Registra una jugada aleatoria
	 */
	public boolean registrarJugadaAleatoria()
	{
		//TODO
        Random gen = new Random();
        int col = gen.nextInt(tablero[0].length - 1);
        for(int i = tablero.length -1;i>=0;i--)
        {
            if(tablero[i][col].equals("___"))
            {
                tablero[i][col] = "_X_";
                return true;
            }
        }
        return false;

	}
	
	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col) //el motodo que llame esto debería llamar terminar despues.
	{
		//TODO
        for(int i = tablero.length -1;i>=0;i--)
        {
            if(tablero[i][col].equals("___"))
            {
                tablero[i][col] = "_"+jugadores.get(turno).darSimbolo()+"_";
                return true;
            }
        }
        return false;
	}
	
	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{
		//TODO
        boolean fin = false;
        boolean horizontal = false;
        boolean vertical = false;
        //1 es para /, 2 es para \
        boolean diagonal1 = false;
        boolean diagonal2 = false;
        //check horizontal
        for (int i = tablero.length -1; i >= 0;i--)
        {
            for (int j = 0;j<tablero[0].length;i++)
            {
                String elSimbolo = tablero[fil][col];
                int contador = fichasParaGanar;
                if(tablero[i][j].equals(elSimbolo))
                {
                    contador--;
                }
                else
                {
                    contador = fichasParaGanar;
                }
                if(contador == 0)
                {
                    fin = false;
                }


            }
        }
        //check vertical
        for(int i = 0;i<tablero[0].length;i++)
        {
            for(int j = tablero.length-1;i>=0;i--)
            {
                String elSimbolo = tablero[fil][col];
                int contador = fichasParaGanar;
                if(tablero[j][i].equals(elSimbolo))
                {
                    contador--;
                }
                else
                {
                    contador = fichasParaGanar;
                }
                if(contador == 0)
                {
                    fin = false;
                }
            }
        }

		return fin;
	}



}
